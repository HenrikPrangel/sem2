package kodutoo6;

import java.util.*;

/**
 * @author HenrikPrangel
 * 
 *         Provided source code has been updated with necessary classes and methods to
 *         find the best width path between a starting vertex and all other
 *         vertexes
 */

/**
 * Container class to different classes, that makes the whole set of classes one
 * class formally.
 */
public class GraphTask {

	/** Main method. */
	public static void main(String[] args) {
		GraphTask a = new GraphTask();
		a.run();
	}

	/** Actual main method to run examples and everything. */
	public void run() {
		 Graph g = new Graph("G");
		 g.createRandomSimpleGraph(4, 6);
		 System.out.println(g);
		 System.out.println(ModifiedDijkstra.outputWidths(g, g.firstVertex));

	}

	class Vertex {

		private String id;
		private Vertex nextVertex;
		private Arc firstArc;
		private int info = 0;
		private int sourceWidth;
		private Vertex previousVertex;

		Vertex(String s, Vertex v, Arc e) {
			id = s;
			nextVertex = v;
			firstArc = e;
		}

		Vertex(String s) {
			this(s, null, null);
		}

		@Override
		public String toString() {
			return id;
		}

	}

	/**
	 * Arc represents one arrow in the graph. Two-directional edges are
	 * represented by two Arc objects (for both directions).
	 */
	class Arc {

		private String id;
		private Vertex targetVertex;
		private Arc nextArc;
		private int width = 0; // Changed "info" to "width"

		Arc(String s, Vertex v, Arc a, int width) {
			this.id = s;
			this.targetVertex = v;
			this.nextArc = a;
			this.width = width;
		}

		Arc(String s, int width) {
			this(s, null, null, width);
		}

		@Override
		public String toString() {
			return id + " (Width: " + width + ") ";
		}
	}

	class Graph {

		private String id;
		private Vertex firstVertex;
		private int info = 0;

		Graph(String s, Vertex v) {
			id = s;
			firstVertex = v;
		}

		Graph(String s) {
			this(s, null);
		}

		@Override
		public String toString() {
			String nl = System.getProperty("line.separator");
			StringBuffer sb = new StringBuffer(nl);
			sb.append(id);
			sb.append(nl);
			Vertex v = firstVertex;
			while (v != null) {
				sb.append(v.toString());
				sb.append(" -->");
				Arc a = v.firstArc;
				while (a != null) {
					sb.append(" ");
					sb.append(a.toString());
					sb.append(" (");
					sb.append(v.toString());
					sb.append("->");
					sb.append(a.targetVertex.toString());
					sb.append(")");
					a = a.nextArc;
				}
				sb.append(nl);
				v = v.nextVertex;
			}
			return sb.toString();
		}

		public Vertex createVertex(String vid) {
			Vertex res = new Vertex(vid);
			res.nextVertex = firstVertex;
			firstVertex = res;
			return res;
		}

		public Arc createArc(String aid, Vertex from, Vertex to, int width) {
			Arc res = new Arc(aid, width);
			res.nextArc = from.firstArc;
			from.firstArc = res;
			res.targetVertex = to;
			res.width = width;
			return res;
		}

		/**
		 * Create a connected undirected random tree with n vertices. Each new
		 * vertex is connected to some random existing vertex.
		 * 
		 * @param n
		 *            number of vertices added to this graph
		 */
		public void createRandomTree(int n) {
			if (n <= 0)
				return;
			Vertex[] varray = new Vertex[n];
			for (int i = 0; i < n; i++) {
				varray[i] = createVertex("v" + String.valueOf(n - i));
				if (i > 0) {
					int vnr = (int) (Math.random() * i);
					// Added random width to original code
					createArc("a" + varray[vnr].toString() + "_" + varray[i].toString(), varray[vnr], varray[i],
							(int) (Math.random() * 50 + 1));
					// Added random width to original code
					createArc("a" + varray[i].toString() + "_" + varray[vnr].toString(), varray[i], varray[vnr],
							(int) (Math.random() * 50 + 1));
				} else {
				}
			}
		}

		/**
		 * Create an adjacency matrix of this graph. Side effect: corrupts info
		 * fields in the graph
		 * 
		 * @return adjacency matrix
		 */
		public int[][] createAdjMatrix() {
			info = 0;
			Vertex v = firstVertex;
			while (v != null) {
				v.info = info++;
				v = v.nextVertex;
			}
			int[][] res = new int[info][info];
			v = firstVertex;
			while (v != null) {
				int i = v.info;
				Arc a = v.firstArc;
				while (a != null) {
					int j = a.targetVertex.info;
					res[i][j]++;
					a = a.nextArc;
				}
				v = v.nextVertex;
			}
			return res;
		}

		/**
		 * Create a connected simple (undirected, no loops, no multiple arcs)
		 * random graph with n vertices and m edges.
		 * 
		 * @param n
		 *            number of vertices
		 * @param m
		 *            number of edges
		 */
		public void createRandomSimpleGraph(int n, int m) {
			if (n <= 0)
				return;
			if (n > 2500)
				throw new IllegalArgumentException("Too many vertices: " + n);
			if (m < n - 1 || m > n * (n - 1) / 2)
				throw new IllegalArgumentException("Impossible number of edges: " + m);
			firstVertex = null;
			createRandomTree(n); // n-1 edges created here
			Vertex[] vert = new Vertex[n];
			Vertex v = firstVertex;
			int c = 0;
			while (v != null) {
				vert[c++] = v;
				v = v.nextVertex;
			}
			int[][] connected = createAdjMatrix();
			int edgeCount = m - n + 1; // remaining edges
			while (edgeCount > 0) {
				int i = (int) (Math.random() * n); // random source
				int j = (int) (Math.random() * n); // random target
				if (i == j)
					continue; // no loops
				if (connected[i][j] != 0 || connected[j][i] != 0)
					continue; // no multiple edges
				Vertex vi = vert[i];
				Vertex vj = vert[j];
				// Added random width to original code
				createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj, (int) (Math.random() * 50 + 1));
				connected[i][j] = 1;
				// Added random width to original code
				createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi, (int) (Math.random() * 50 + 1));
				connected[j][i] = 1;
				edgeCount--; // a new edge happily created
			}
		}

		/**
		 * Method to create an an array list of vertexes
		 * 
		 * @return Created array list of vertexes
		 */
		public ArrayList<Vertex> getVertexList() {
			ArrayList<Vertex> res = new ArrayList<Vertex>();
			Vertex v = firstVertex;
			while (v != null) {
				res.add(v);
				v = v.nextVertex;
			}
			return res;
		}
	}

	/**
	 * Class ModifiedDijkstra, containing methods to find the best width path in
	 * a given graph, and outputting said paths.
	 */
	static class ModifiedDijkstra {

		// Assisting method to find the smaller input
		private static int min(int i, int o) {
			if (i > o) {
				return o;
			} else
				return i;
		}

		// Assisting method to find the larger input
		private static int max(int i, int o) {
			if (i < o) {
				return o;
			} else
				return i;
		}

		/**
		 * Finds the best width path from given vertex to other vertexes, using
		 * a modified Dijkstra method
		 * 
		 * @param graph
		 *            Input graph for which we are going to find best width
		 *            paths
		 * @param startingVertex
		 *            Vertex from which we start finding best width paths
		 */
		private static void generateMaxMinPath(Graph graph, Vertex startingVertex) {

			// Error handling incase input graph is empty
			if (graph.firstVertex == null) {
				throw new RuntimeException("Error occured in method generateMaxMinPath, input graph is empty");
			}

			// Method begins by defining the width of all vertixes to negative
			// infinity (or a value sufficiently small enough) and sets the
			// previous vertex, of currently handled vertex, to undefined
			Vertex v = graph.firstVertex;
			while (v != null) {
				v.sourceWidth = Integer.MIN_VALUE / 4;
				v.previousVertex = null;
				v = v.nextVertex;
			}
			// Set starting vertex width to infinity (or another value
			// sufficiently large enough) and previous vertex to starting vertex
			startingVertex.sourceWidth = Integer.MAX_VALUE / 4;
			startingVertex.previousVertex = startingVertex;
			// Generate an arraylist to hold all vertixes
			ArrayList<Vertex> Q = graph.getVertexList();
			while (!Q.isEmpty()) {
				// For as long, as our vertex list contains vertexes, we remove
				// a vertex to handle it and its arc's
				int i = Q.size();
				Vertex largestWidthVertex = Q.get(i - 1);
				Q.remove(i - 1);
				Arc arc = largestWidthVertex.firstArc;
				while (arc != null) {
					// We find the new width between source (currently handled
					// vertex) and target vertex (target vertex of currently
					// handled vertex)
					int wWidth = max(arc.targetVertex.sourceWidth, min(largestWidthVertex.sourceWidth, arc.width));
					// If new found width is better than what we already have,
					// then we use that instead
					if (arc.targetVertex.sourceWidth < wWidth) {
						arc.targetVertex.sourceWidth = wWidth;
						// Remove and insert vertex back into the array list
						Q.remove(arc.targetVertex);
						Q.add(arc.targetVertex);
						// Update path info
						arc.targetVertex.previousVertex = largestWidthVertex;
					}
					// Handle the next arc, if there still are any
					arc = arc.nextArc;
				}
			}
			// Define the width of the staring vertex to 0, so that the
			// outputted path would be easier to read
			startingVertex.sourceWidth = 0;
			return;
		}

		/**
		 * Outputs a graph, that has been updated with best width path info
		 * 
		 * @param graph
		 *            Graph that has been updated with best width path info
		 * @param startingVertex
		 *            Starting vertex for which we are going to output optimal
		 *            paths
		 * @return Output of optimal paths
		 */
		private static String outputModDijkstra(Graph graph, Vertex startingVertex) {

			// Error handling in case input graph has not been updated with
			// necessary info
			if (startingVertex.previousVertex == null) {
				throw new RuntimeException("Inputted graph has not been updated with best width path info");
			}
			String nl = System.getProperty("line.separator");
			StringBuffer sb = new StringBuffer(nl);
			sb.append("Dijkstra for Widths");
			sb.append(nl);
			sb.append(graph.id);
			sb.append(nl);
			Vertex v = graph.firstVertex;
			while (v != null) {
				sb.append(startingVertex.toString());
				sb.append(" -->");
				sb.append(v.toString());
				sb.append(" (Width: " + v.sourceWidth + ") --> ");
				StringBuffer sbPath = new StringBuffer();
				Vertex pathTrack = v;

				do {
					sbPath.insert(0, pathTrack.previousVertex.toString() + "-(W: " + pathTrack.sourceWidth + ")->"
							+ pathTrack.toString() + "   ");
					pathTrack = pathTrack.previousVertex;
				} while (pathTrack != startingVertex);
				sb.append(sbPath);
				sb.append(nl);
				v = v.nextVertex;
			}
			return sb.toString();
		}

		/**
		 * Method to be used for finding the best width path info for a graph
		 * Method finds the best width path info and then also output's it
		 * 
		 * @param graph
		 *            Graph for which we want to find the best width path info
		 * @param startingVertex
		 *            Vertex from which we want to find the best path's to other
		 *            vertexes
		 * @return Output of optimal paths
		 */
		public static String outputWidths(Graph graph, Vertex startingVertex) {
			generateMaxMinPath(graph, startingVertex);
			return outputModDijkstra(graph, startingVertex);
		}
	}
}
