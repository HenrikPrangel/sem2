package kodutoo1;

import java.util.Arrays; //toString() debug ridade jaoks

public class BallsaBackup {

	enum Color {
		green, red, blue
	};

	public static void main(String[] param) {
		// for debugging
		Color arr[] = new Color[] { Color.green, Color.green, Color.blue, Color.red,
				Color.green, Color.green, Color.green, Color.red, Color.green,
				Color.red, Color.red, Color.red };
		reorder(arr);
		System.out.println(Arrays.toString(arr));

		arr = new Color[] { Color.blue, Color.green, Color.green, Color.green, Color.green,
				Color.green, Color.green, Color.red, Color.blue, Color.red,
				Color.red, Color.red, Color.red };
		reorder(arr);
		System.out.println(Arrays.toString(arr));

	}

	public static void reorder(Color[] balls) {

		// reorderPistemeetod(balls);
		reorderPistemeetod2(balls);

	}

	/**
	 * Pistemeetodist aretatud sort. Läbib jada vasakult paremale.
	 * Mittestabiilne.
	 * 
	 * Asendaja - pall, millega asendame. Asendatav - pall, mille vahetame välja
	 * asendajaga.
	 * 
	 * Esimene ettejuhtuv mittesobiv (asendatav) asendatakse otsides kaugemalt
	 * jadast paremale poole liikudes sobivaid asendajaid. Asendaja leidmisel
	 * jätkatakse otsimist sealt, kust viimati/eelmises tsüklik asendaja leiti
	 * (mitte järgnevast kohast, kus asendatav pall leiti)
	 * 
	 * @see http://enos.itcollege.ee/~jpoial/algoritmid/searchsort.html
	 */
	@SuppressWarnings("unused")
	private static void reorderPistemeetod(Color[] balls) {

		if (balls.length < 2)
			return;

		int idxPunasedOtsitudKuni = 0;

		for (int i = 0; i < balls.length; i++) {

			// System.out.println("i:" + i + "\t" + Arrays.toString(balls));

			Color b = balls[i]; // algusest (vasakult) võtame palle ükshaaval

			if (b == Color.red) {
				continue; // ülesandes oli öeldud, et punane enne, roheline
							// pärast. Seega punane pall on õiges kohas
			} else {
				;
				// saime rohelise palli. See tuleks tõsta esimesse ettejuhtuva
				// punase asemel (hiljem võib ette tulla, et on vaja uuesti
				// lõpupoole visata)
				// läheme paremalt otsima esimest ettejuhtuvat punast palli,
				// millega välja vahetada
			}

			// otsime jadas paremale poole liikudes teist värvi palli kohta,
			// millega asendada/ringi tõsta.
			// Otsimist alustame sealt, kuhumaani eelmise tsükliga sai läbi
			// vaadatud,
			// sellest eestpoolt ei ole mõttet teist korda vaadata
			int j = Math.max(i, idxPunasedOtsitudKuni + 1); // for tsükli
															// algväärtustamise
															// teeme siin kuna
															// tsükli lõpus
															// läheb muutujat
															// "j" vaja
			for (; j < balls.length; j++) {
				// System.out.println("\t\tj: "+j);
				if (balls[j] == Color.red) {

					// swap
					Color tmp = balls[i];
					balls[i] = balls[j];
					balls[j] = tmp;

					idxPunasedOtsitudKuni = j;
					break;
				}
			}

			// lõpuni otsitud. Rohkem sorteerida ei saa kuna
			// ringitõstmiseks lõpupool punaseid palle ei ole enam võtta
			if (j == balls.length) {
				return;
			}

		}

	}

	/**
	 * 
	 * sama, mis reorderPistemeetod(Color[] balls) kuid sisemine tsükkel otsib
	 * asendajaid paremalt vasakule (tagant ette tulles).
	 * 
	 * reorderPistemeetod(Color[] balls) viga on selles, et rohelisi ei visata
	 * täitsa lõppu vaid kuskile vahepeale enamasti, siis võib juhtuda, et neid
	 * on vaja veel edasi "visata" paremale poole. Halvimal juhul on vaja visata
	 * balls.lenght-1 korda kui massiiv on {green, red, red...red}
	 * 
	 * Tsükkel loetakse lõpetatuks kui sisemine tsükkel jõuab välimise tsükli
	 * asukohani. Siis on välimise tsükli vasakul pool punased, sisemise tsükli
	 * paremal pool rohelised.
	 * 
	 * @param balls
	 */

	private static void reorderPistemeetod2(Color[] balls) {

		if (balls.length < 2)
			return;

		// int idxPunasedOtsitudKuni = 0;
		int j = balls.length - 1;

		for (int i = 0; i < balls.length; i++) {

			// System.out.println("i:" + i + "\t" + Arrays.toString(balls));

			Color b = balls[i]; // algusest (vasakult) võtame palle ükshaaval

			if (b == Color.red) {
				continue; // ülesandes oli öeldud, et punane enne, roheline
							// pärast. Seega punane pall on õiges kohas
			} else {
				;
				// saime rohelise palli. Nüüd tuleks otsida lõpust (paremalt)
				// ühte punast palli, millega ära vahetada (mittestabiilne)
			}

			// otsime jadas paremale poole liikudes teist värvi palli kohta,
			// millega asendada/ringi tõsta.
			// Otsimist alustame sealt, kuhumaani eelmise tsükliga sai läbi
			// vaadatud,
			// sellest eestpoolt ei ole mõttet teist korda vaadata

			for (; j >= i && j >= 0; j--) { // Kui jõuab paremalt tulles
											// vasakule poole
				// "int i" väärtuseni, siis on otsimine
				// läbi. "i"-st vasakule ei saa rohelist
				// palli võtta

				// System.out.println("\t\tj: "+j);
				if (balls[j] == Color.red) {

					// swap
					Color tmp = balls[i];
					balls[i] = balls[j];
					balls[j] = tmp;

					break;
				}
			}

			// sorditud. i-st vasakul punased, j paremal rohelised. j-il ei ole
			// mõttet minna i-st vasakule punaste seast rohelisi otsima
			if (j <= i) {
				return;
			}

		}

	}

}
