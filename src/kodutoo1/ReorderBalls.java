package kodutoo1;

import java.util.Arrays; 

public class ReorderBalls {

	enum Color {
		green, red, blue
	};

	public static void main(String[] args) {

		long startTime = System.nanoTime();
		
		
		Color pallid[] = new Color[] { Color.green, Color.green, Color.blue, Color.red, Color.green, Color.green,
				Color.green, Color.red, Color.blue, Color.green, Color.red, Color.red, Color.red, Color.blue, Color.red,
				Color.green, Color.blue };

		sorteeriPallid(pallid);
		System.out.println(Arrays.toString(sorteeriPallid(pallid)));
		
		
		long endTime = System.nanoTime();
		System.out.println("Took " + (endTime - startTime) + " ns");

	}

//	public static void sorteeri(Color[] pallid) {
//
//		sorteeriPallid(pallid);
//
//	}

	private static Color[] sorteeriPallid(Color[] pallid) {

		int x = 0;
		int y = 0;
		int z = 0;

		for (int i = 0; i < pallid.length; i++) {

			Color vaadeldav = pallid[i];

			if (vaadeldav == Color.red) {
				x += 1;
			} else if (vaadeldav == Color.green) {
				y += 1;
			} else if (vaadeldav == Color.blue) {
				z += 1;
			}

		}

		Color sorPallid[] = new Color[x+y+z];
		
		for (int i = 0; i < sorPallid.length; i++) {
			if (i<x) {
				sorPallid[i]=Color.red;
				continue;
			}
			if (i<x+y) {
				sorPallid[i]=Color.green;
				continue;
				
			}
			if (i<x+y+z) {
				sorPallid[i]=Color.blue;
				continue;
			}
		}
//		System.out.println(Arrays.toString(sorPallid));

		return sorPallid;

	}

}
