package kodutoo1;

import java.util.Arrays;

public class ReorderBallColors {

	enum Color {
		green, red, blue
	};

	public static void main(String[] param) {
		// for debugging
		long startTime = System.nanoTime();

		Color balls[] = new Color[] { Color.green, Color.green, Color.blue, Color.red, Color.green, Color.green,
				Color.green, Color.red, Color.blue, Color.green, Color.red, Color.red, Color.red, Color.blue, Color.red,
				Color.green, Color.blue };
		
		reorder(balls);
		System.out.println(Arrays.toString(balls));

		long endTime = System.nanoTime();
		System.out.println("Took " + (endTime - startTime) + " ns");

	}

	public static void reorder(Color[] balls) {

		reorderBalls(balls, Color.red, Color.red);
		reorderBalls(balls, Color.red, Color.green);

	}

	private static void reorderBalls(Color[] balls, Color x, Color y) {

		if (balls.length < 2)
			return;

		int j = balls.length - 1;

		for (int i = 0; i < balls.length; i++) {

			Color b = balls[i];

			if (b == x || b == y) {
				continue;
			} 

			for (; j >= i && j >= 0; j--) {

				if (balls[j] == y) {

					Color tmp = balls[i];
					balls[i] = balls[j];
					balls[j] = tmp;

					break;
				}
			}
			if (j <= i) {
				return;
			}
		}
	}

}

// VIIDE: https://drive.google.com/drive/folders/0B8p16Nvt8x8WS21qTjAyWEU0NEE
// Koodil�ik sorteerimiseks sai osaliselt v�etud EIK Kaug�ppe kodut��
// materjalist
