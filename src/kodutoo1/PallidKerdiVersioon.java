package kodutoo1;
import java.util.Arrays;
import java.util.Random;

public class PallidKerdiVersioon{

enum Colors {
	red, green, blue
	}

	public static void main(String[] args) {
		Colors[] balls = new Colors[12];

		Colors[] values = Colors.values();
		Random rand = new Random();
		for (int i = 0; i < balls.length; i++)
			balls[i] = values[rand.nextInt(values.length)];


		Sorting(balls);

		System.out.println(Arrays.toString(balls));
	}


	private static void swap(Colors[] arry, int a, int b) {
		Colors tmp = arry[a];
		arry[a] = arry[b];
		arry[b] = tmp;
	}

	private static void Sorting(Colors[] balls) {
		int low = 0, middle = 0, high = balls.length - 1;

		while (middle <= high)
			switch (balls[middle]) {
				case red:
					swap(balls, low++, middle++);
					break;
				case green:
					middle++;
					break;
				case blue:
					swap(balls, middle, high--);
					break;
			}
	}


}