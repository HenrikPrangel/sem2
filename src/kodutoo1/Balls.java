// $Id: Balls.java 345 2013-02-18 21:43:33Z ylari $

package kodutoo1;

import java.util.Arrays;
import java.util.Collections;

/**
 * An array contains red and green balls in random order. Write a possibly fast method to rearrange the array,
 * so that all red balls are at the beginning and all green balls are at the end of the array. Consider the case
 * when all balls have the same color.
 * @author <a href="mailto:ulari.ainjarv@itcollege.ee?subject=I231: Balls.java ...">Ülari Ainjärv</a>
 * @version $Revision: 345 $
 */
public class Balls {
	
	
	public static void main(String[] args) {
		
	}

	enum Color {
		green, red
	};

	public static void reorder(Color[] balls) {
		// built(balls); // too slow
		// quick(balls, 0, balls.length - 1); // too slow
		simple(balls); // ~3 ms
	}

	/**
	 * Built in sorting algorithm.
	 * @param balls an array which needs reordering.
	 */
	@SuppressWarnings("unused")
	private static void built(Color[] balls) {
		Arrays.sort(balls, Collections.reverseOrder());
	}

	/**
	 * Quick sorting algorithm (with an in-place partitioning).
	 * @param balls an array which needs reordering.
	 * @param left index of the leftmost element of the array.
	 * @param right index of the rightmost element of the array.
	 */
	@SuppressWarnings("unused")
	private static void quick(Color[] balls, int left, int right) {
		if (left <= right) {
			int i = left, j = right;
			Color pivot = balls[(left + right) / 2], tmp;
			while (i <= j) {
				while (balls[i].ordinal() > pivot.ordinal())
					i++;
				while (balls[j].ordinal() < pivot.ordinal())
					j--;
				if (i <= j) {
					tmp = balls[i];
					balls[i] = balls[j];
					balls[j] = tmp;
					i++;
					j--;
				}
			}
			if (left < j)
				quick(balls, left, j);
			if (i < right)
				quick(balls, i, right);
		}
	}

	/**
	 * Simple sorting algorithm.
	 * @param balls an array which needs reordering.
	 */
	private static void simple(Color[] balls) {
		int i = 0, j;
		for (Color ball : balls)
			if (ball.ordinal() > 0) // not first, probably red
				i++;
		for (j = 0; j < i; j++)
			balls[j] = Color.red;
		for (i = j; i < balls.length; i++)
			balls[i] = Color.green;
	}

}
