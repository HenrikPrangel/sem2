package kodutoo1;

import java.util.Arrays; 

public class PallidMarkuseversioon {

	enum Color { green, red, blue };
	
	public static void main(String[] args) {
		
		Color pallid[] = new Color[] { Color.green, Color.green, Color.blue, Color.red, Color.green, Color.green,
				Color.green, Color.red, Color.blue, Color.green, Color.red, Color.red, Color.red, Color.blue, Color.red,
				Color.green, Color.blue };
		
		sorteeriPallid(pallid);
		
		System.out.println(Arrays.toString((pallid)));
		}
	
	private static void sorteeriPallid (Color[] pallid) {
		
		int a = 0, b = 0, c = pallid.length - 1;
		
		while (b <= c)
			switch (pallid[b]) {
			case red:
				vahetus(pallid, a++, b++);
				break;
			case green:
				b++;
				break;
			case blue:
				vahetus(pallid, b, c--);
				break;
			}
		
	}
	private static void vahetus(Color [] pallid, int a, int b) {
		Color tmp = pallid[a];
		pallid[a] = pallid[b];
		pallid[b] = tmp;
	}
}