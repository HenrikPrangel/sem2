package kodutoo5;

import java.util.*;

public class TreeNode {

   private String name;
   private TreeNode firstChild;
   private TreeNode nextSibling;
   public static boolean alreadyExecuted = false;

   public TreeNode (String n, TreeNode d, TreeNode r) {
      name = n;
      firstChild = d;
      nextSibling = r;
   } 
   
   public static void ValidStringCheck(String s) {
	   if (s.contains(" ") || s.contains(",,") || s.contains("(,"))
		   throw new RuntimeException("Invalid string, please check: " + s);
	   if (s.contains("(") && s.charAt(s.length() - 1) != ')')
		   throw new RuntimeException("Invalid string: " + s);
	   if (s.contains("()"))
		   throw new RuntimeException("Tree: \"" + s + "\" contains an empty subtree");
	   int nrCnt1 = 0, nrCnt2 = 0;	
	   for (int i = 0; i < s.length(); i++) {		   	   
		   if (s.charAt(i) == '(')
			   nrCnt1 += 1;
		   if (s.charAt(i) == ')') {
			   nrCnt2 += 1;
			   if (nrCnt1 == nrCnt2 && i < s.length() - 1)
				   throw new RuntimeException("Invalid string: " + s);
		   }
	   }
	   if (nrCnt1 != nrCnt2)
		   throw new RuntimeException("Uneven amount of brackets in string: " + s);
	   if (s.contains(",") && !s.contains("("))
		   throw new RuntimeException("Invalid string: " + s);
	   char c = s.charAt(0);
	   if (!Character.isLetterOrDigit(c) && c != '*' && c != '/' && c != '+' && c != '-')
		   throw new RuntimeException("Invalid string: " + s);
	   alreadyExecuted = true;
   }
   
   public static TreeNode parsePrefix (String s) {
	   if (!alreadyExecuted)
		   ValidStringCheck(s);
	   if (s == null || s.isEmpty() )
		   return null;
	   StringTokenizer part = new StringTokenizer(s,",()",true);
	   String name = part.nextToken();
	   
//		System.out.println("name = " + name); ////////////////////
		
		if (name == null || name.isEmpty() || name.contains("(") || name.contains(")") || name.contains(",") || name.contains(" ")) {
			throw new RuntimeException("Your input " + s + " is inproper (name)");
		}
	   String Q = "", E = "", V = "";  
	   while(part.hasMoreTokens())	{
		   Q = part.nextToken();		   
		   if(Q.equals("(")) {	
			   Q = part.nextToken();
				
//				System.out.println("Q = " + Q); ///////////////////
				

			   if (Q.equals("("))
				   throw new RuntimeException("Invalid string: " + s);
			   int closed = 0; 
			   while(closed < 1) {
				   E = E + Q;	//firstChild
				   Q = part.nextToken();
				   if(Q.equals("("))
					   closed = closed - 1;
				   else if(Q.equals(")"))
					   closed = closed + 1;
			   }
		   }
		   if (Q.equals(",")) 
			   while(part.hasMoreTokens()) 
				   V = V + part.nextToken(); //nextSibling		 	   
	   }
	   return new TreeNode(name,TreeNode.parsePrefix(E),TreeNode.parsePrefix(V));
   }

   public String rightParentheticRepresentation() {
      StringBuilder result = new StringBuilder();
      if (firstChild != null) {
    	  result.append('(');
    	  result.append(firstChild.rightParentheticRepresentation());
    	  result.append(')');
      }
    	  result.append(name);       
      if (nextSibling != null) {
    	  result.append(',');
    	  result.append(nextSibling.rightParentheticRepresentation());
      }
      alreadyExecuted = false;
      return result.toString();
   }

   public static void main (String[] param) {

	      String s = "A(B1,C,D)";
	      TreeNode t = TreeNode.parsePrefix (s);
	      String v = t.rightParentheticRepresentation();
	      System.out.println (s + " ==> " + v); // A(B1,C,D) ==> (B1,C,D)A
	      
		
		System.out.println("-------------------------------------------------");
		
//	    s = "A,B";
//		s = "A(B),C(D)";
//		s = "A(B,C),D";
//		s = "\t";
	      
//	      s = "A B";
//	      s = "A(B,,C)";
//	      s = "A()";
//	      s = "A,B)";
//	      s = "A((C,D))";
//	      s = "A(,B)";
//	      s = ")A(";

	      
	      System.out.println("string s on: " + s);
	      t = TreeNode.parsePrefix (s);
	      v = t.rightParentheticRepresentation();
	      System.out.println (s + " ==> " + v); 
	      

////
//s = "A(B1,C,D)";
//t = TreeNode.parsePrefix (s);
//String r = t.rightParentheticRepresentation();
////assertEquals ("Tree: " + s, "(B1,C,D)A", r);
//
// s = "2";
// t = TreeNode.parsePrefix (s);
// r = t.rightParentheticRepresentation();
//// assertEquals ("Tree: " + s, "2", r);
// 
//  System.out.println (s + " ==> " + r);
// 
// s = "+(*(-(2,1),4),/(6,3))";
// t = TreeNode.parsePrefix (s);
// r = t.rightParentheticRepresentation();
//// assertEquals ("Tree: " + s, "(((2,1)-,4)*,(6,3)/)+", r);
// System.out.println (s + " ==> " + r);
//
//
//
//  s = "A(B(C(D(E))))"; 
//  t = TreeNode.parsePrefix (s);
//  r = t.rightParentheticRepresentation();
//// assertEquals ("Tree: " + s, "((((E)D)C)B)A", r);
// System.out.println (s + " ==> " + r);
//  
// s = "A(B(C,D(E)),F)";
// t = TreeNode.parsePrefix (s);
// r = t.rightParentheticRepresentation();
//// assertEquals ("Tree: " + s, "((C,(E)D)B,F)A", r);
// System.out.println (s + " ==> " + r);
//
//
//
// s = "+(*(-(512,1),4),/(-6,3))";
// t = TreeNode.parsePrefix (s);
// r = t.rightParentheticRepresentation();
//// assertEquals ("Tree: " + s, "(((512,1)-,4)*,(-6,3)/)+", r);
// System.out.println (s + " ==> " + r);
// 
// s = "A(B,C,D,E,F)";
// t = TreeNode.parsePrefix (s);
// r = t.rightParentheticRepresentation();
//// assertEquals ("Tree: " + s, "(B,C,D,E,F)A", r);
// System.out.println (s + " ==> " + r);
// 
// s = "6(5(1,3(2),4))";
// t = TreeNode.parsePrefix (s);
// r = t.rightParentheticRepresentation();
////  assertEquals ("Tree: " + s, "((1,(2)3,4)5)6", r);
// System.out.println (s + " ==> " + r);
// 
// s = "ABC";
// t = TreeNode.parsePrefix (s);
// r = t.rightParentheticRepresentation();
//// assertEquals ("Tree: " + s, "ABC", r);
// System.out.println (s + " ==> " + r);
// 
// s = ".Y.";
// t = new TreeNode (s, null, null);
// r = t.rightParentheticRepresentation();
//// assertEquals ("Single node" + s, s, r);
// System.out.println (s + " ==> " + r);
	      
   }
}

//https://gist.github.com/not-much-io/01c6abfd14a9df1fefb7
//Viide: Suur osa koodiloogikast on v�etud ka v�etud varasema aasta ITK tudengi materjalist.








