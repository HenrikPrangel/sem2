package kodutoo3;

import java.util.LinkedList;

public class DoubleStack {

	private LinkedList<Double> list;

	public static void main(String[] argum) {

		//// Kontrolliks ja testimiseks
		DoubleStack vana = new DoubleStack();
		vana.push(0.1);
		vana.push(3.2);
		vana.push(1.2);
		vana.push(1.4);
		System.out.println("Equals meetodi kontroll: " + vana.equals(vana));
		System.out.println("toString meetodi kontroll: " + vana);
		System.out.println("tos meetodi kontroll: " + vana.tos());
		vana.pop();
		System.out.println("pop meetodi kontroll: " + vana);
		vana.op("+");

		System.out.println("op meetodi kontrollimine, kasutades + m�rki: " + vana);
		System.out.println("interpret meetodi kontrollimine: " + DoubleStack.interpret("2. 15. - "));
		System.out.println("interpret meetodi kontrollimine: " + DoubleStack.interpret("2. 15. - 3. * 1. - 2. /"));
//		System.out.println(DoubleStack.interpret("1. - ")); 
		System.out.println("interpret meetodi kontrollimine: " + DoubleStack.interpret("1 2 +"));

		//// P�hjalikum clone meetodi kontrollimine
		// DoubleStack uus = (DoubleStack) vana.clone();
		// System.out.println(uus);
		// System.out.println(vana.equals(uus));

	}

	DoubleStack() {
		this.list = new LinkedList<Double>();
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		DoubleStack tmp = new DoubleStack();
		for (int i = 0; i < list.size(); i++) {
			tmp.list.add(list.get(i));
		}
		return tmp;
	}

	public boolean stEmpty() {
		if (list.isEmpty() == true) {
			return true;
		} else
			return false;
	}

	public void push(double a) {
		list.add(a);
	}

	public double pop() {
		if (stEmpty()) {
			throw new IndexOutOfBoundsException("magasini alataitumine");
		}
		double a = list.get(list.size() - 1);
		list.remove(list.size() - 1);
		return a;
	}

	public void op(String input) {
		if (stEmpty()) {
			throw new IndexOutOfBoundsException("magasini alataitumine");
		}
		double x = pop(), y = pop();

		if (input.equals("*")) {
			list.add(x * y);
		}
		if (input.equals("/")) {
			list.add(y / x);
		}
		if (input.equals("-")) {
			list.add(y - x);
		}
		if (input.equals("+")) {
			list.add(x + y);
		}

	}

	public double tos() {
		if (stEmpty()) {
			throw new IndexOutOfBoundsException("magasini alataitumine");
		}
		return list.get(list.size() - 1);
	}

	@Override
	public boolean equals(Object o) {
		return list.equals(((DoubleStack) o).list);
	}

	@Override
	public String toString() {
		if (stEmpty())
			return "Tyhi";
		StringBuffer tmp = new StringBuffer();
		for (int i = 0; i <= list.size() - 1; i++)
			tmp.append(String.valueOf(list.get(i) + "   "));
		return tmp.toString();

	}

    public static double interpret(String pol) {
    	 
        if (!pol.matches(".*\\d+.*")) {
            throw new IllegalArgumentException("Input: \""  + pol + "\" is missing numbers");
        }
       
        String[] jupid = pol.trim().split("\\s+");
        if (!pol.contains("+") && !pol.contains("-") && !pol.contains("*") && !pol.contains("/") && jupid.length > 1){
            throw new IllegalArgumentException("Input: \""  + pol + "\" is missing mathematical function symbols");
        }
        int nrCnt = 0;
        DoubleStack tmp = new DoubleStack();
        for (int i = 0; i < jupid.length; i++) {
            if (isDouble(jupid[i])) {
                tmp.push(Double.parseDouble(jupid[i]));
                nrCnt++;
            } else {
                try {
                   tmp.op(jupid[i]);
                } catch (RuntimeException e) {
                    throw new IllegalArgumentException("Input is not proper : " + pol );
                }
            }
        }
       
        if (nrCnt == (jupid.length - nrCnt) + 1) {
            return tmp.tos();
        }
        throw new RuntimeException("Input: " + pol + "contains too few arguments");
    }

	public static boolean isDouble(String txt) {
		try {
			Double.parseDouble(txt);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

}

// viited: eeskuju enamikeks meetoditeks sai v�etud �ppej�u materjalist -
// http://enos.itcollege.ee/~jpoial/algoritmid/adt.html -
// interpret ja clone meetod said v�etud varasema ITK tudengi materjalist
// https://pastebin.com/YKFtGbhc
// https://stackoverflow.com/questions/3133770/how-to-find-out-if-the-value-contained-in-a-string-is-double-or-not