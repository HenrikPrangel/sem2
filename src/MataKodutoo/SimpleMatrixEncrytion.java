package MataKodutoo;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;

public class SimpleMatrixEncrytion {

	public static void main(String args[]) {
		
		//Viited:
		//P66rdmaatriksi leidmiseks kasutati koodi netist, antud lingilt:
		//http://www.sanfoundry.com/java-program-find-inverse-matrix/

		System.out.println("Palun sisesta lause, mida soovid krypteerida: ");
		System.out.println("NB: teksti sisestades ei tohi kasutada numbreid ega m2rke/symboleid. Samuti ei tohi kasutada Eesti t2hestiku t2hti");
		Scanner inputh = new Scanner(System.in);
		String input = "";
		input += inputh.nextLine();
		inputh.close();
		
		String[] u = getString(input);
		double[][] key = { { 4, 3 }, { 5, -3 } };
		HashMap<String, Integer> hm = MakeHashMap();
		int[] a = bindStringToHashMapValues(u, hm);
		
		System.out.println("Krypteerituna n2eb sinu lause v2lja j2rgnevalt: ");
		double[] encrypted = encrypt(a, key);		
		output(encrypted);
		
		System.out.println();
		System.out.println("Krypteeritud rida dekrypteeritult: ");
		double[] decrypted = decrypt(encrypted, key);
		outputAsText(decrypted, hm);

	}

	public static double[] encrypt(int[] a, double[][] key) { //Method that encrypts an input int array

		double[] encrypted = new double[a.length];

		for (int i = 0; i < a.length; i++) {

			if (i % 2 == 0) {
				encrypted[i] = a[i] * key[0][0] + a[i + 1] * key[1][0];
			} else {
				encrypted[i] = a[i - 1] * key[0][1] + a[i] * key[1][1];
			}
		}
		return encrypted;
	}

	public static double[] decrypt(double[] encrypted, double[][] key) { // method that decrypts an input encrypted array

		double[] decrypted = new double[encrypted.length];
		double[][] invKey = invert(key);

		for (int i = 0; i < encrypted.length; i++) {

			if (i % 2 == 0) {
				decrypted[i] = Math.round(encrypted[i] * invKey[0][0] + encrypted[i + 1] * invKey[1][0]);
			} else {
				decrypted[i] = Math.round(encrypted[i - 1] * invKey[0][1] + encrypted[i] * invKey[1][1]);
			}
		}
		return decrypted;

	}

	public static void output(double[] tooutput) { // Method used for outputting encrypted/decrypted arrays.

		for (int i = 0; i < tooutput.length; i++) {
			System.out.print((int) tooutput[i] + " ");
		}
		System.out.println();
	}

	public static void outputAsText(double[] tooutput, HashMap<String, Integer> hm) { // Method that outputs decrypted double array as text

		HashMap<String, Integer> testMapp = hm;

		for (int i = 0; i < tooutput.length; i++) {
			for (Entry<String, Integer> entry : testMapp.entrySet()) {
				if (entry.getValue().equals((int) tooutput[i])) {
					System.out.print(entry.getKey());

				}
			}
		}
	}

	public static int[] bindStringToHashMapValues(String[] u, HashMap<String, Integer> hm) { // Method that assigns int values to string array

		int counter = 0;
		int[] a = new int[(u.length)];

		for (int i = 0; i < a.length; i++) {
			String temp = u[counter];
			a[i] = (int) hm.get(temp);
			counter += 1;
			if (counter == u.length) {
				break;
			}
		}
		return a;

	}

	public static String[] getString(String x) { // Method that creates an array
													// of a string
		if (x.length() % 2 != 0) {
			String[] y = x.split("");
			String[] y2 = new String[y.length + 1];
			for (int i = 0; i < y2.length; i++) {
				if (i == y2.length - 1) {
					y2[i] = " ";
					break;
				} else {
					y2[i] = y[i].toLowerCase();
				}
			}

			return y2;

		} else {
			String[] y = x.split("");
			String[] y2 = new String[y.length];
			for (int i = 0; i < y2.length; i++) {
				y2[i] = y[i].toLowerCase();
			}
			return y2;
		}
	}

	public static HashMap<String, Integer> MakeHashMap() { 

		HashMap<String, Integer> hm = new HashMap<>();
		hm.put(" ", 1);
		hm.put("a", 21);
		hm.put("b", 30);
		hm.put("c", 19);
		hm.put("d", 68);
		hm.put("e", 56);
		hm.put("f", 33);
		hm.put("g", 45);
		hm.put("h", 34);
		hm.put("i", 58);
		hm.put("j", 12);
		hm.put("k", 20);
		hm.put("l", 39);
		hm.put("m", 13);
		hm.put("n", 18);
		hm.put("o", 41);
		hm.put("p", 69);
		hm.put("q", 70);
		hm.put("r", 72);
		hm.put("s", 57);
		hm.put("t", 90);
		hm.put("u", 99);
		hm.put("v", 89);
		hm.put("w", 78);
		hm.put("x", 51);
		hm.put("y", 61);
		hm.put("z", 71);

		return hm;
	}
	
	
	public static double[][] invert(double aw[][]) {
		int n = aw.length;
		double x[][] = new double[n][n];
		double b[][] = new double[n][n];
		int index[] = new int[n];
		for (int i = 0; i < n; ++i)
			b[i][i] = 1;

		// Transform the matrix into an upper triangle
		gaussian(aw, index);

		// Update the matrix b[i][j] with the ratios stored
		for (int i = 0; i < n - 1; ++i)
			for (int j = i + 1; j < n; ++j)
				for (int k = 0; k < n; ++k)
					b[index[j]][k] -= aw[index[j]][i] * b[index[i]][k];

		// Perform backward substitutions
		for (int i = 0; i < n; ++i) {
			x[n - 1][i] = b[index[n - 1]][i] / aw[index[n - 1]][n - 1];
			for (int j = n - 2; j >= 0; --j) {
				x[j][i] = b[index[j]][i];
				for (int k = j + 1; k < n; ++k) {
					x[j][i] -= aw[index[j]][k] * x[k][i];
				}
				x[j][i] /= aw[index[j]][j];
			}
		}
		return x;
	}

	public static void gaussian(double as[][], int index[]) {
		int n = index.length;
		double c[] = new double[n];

		// Initialize the index
		for (int i = 0; i < n; ++i)
			index[i] = i;

		// Find the rescaling factors, one from each row
		for (int i = 0; i < n; ++i) {
			double c1 = 0;
			for (int j = 0; j < n; ++j) {
				double c0 = Math.abs(as[i][j]);
				if (c0 > c1)
					c1 = c0;
			}
			c[i] = c1;
		}

		// Search the pivoting element from each column
		int k = 0;
		for (int j = 0; j < n - 1; ++j) {
			double pi1 = 0;
			for (int i = j; i < n; ++i) {
				double pi0 = Math.abs(as[index[i]][j]);
				pi0 /= c[index[i]];
				if (pi0 > pi1) {
					pi1 = pi0;
					k = i;
				}
			}

			// Interchange rows according to the pivoting order
			int itmp = index[j];
			index[j] = index[k];
			index[k] = itmp;
			for (int i = j + 1; i < n; ++i) {
				double pj = as[index[i]][j] / as[index[j]][j];

				// Record pivoting ratios below the diagonal
				as[index[i]][j] = pj;

				// Modify other elements accordingly
				for (int l = j + 1; l < n; ++l)
					as[index[i]][l] -= pj * as[index[j]][l];
			}
		}
	}
}