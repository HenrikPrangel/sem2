package kodutoo4;

public class Lfraction implements Comparable<Lfraction> {

	public static void main(String[] param) {
		Lfraction Juhan = new Lfraction(2, -5);
//		Lfraction Juhan2 = new Lfraction(-4, 7);
		Lfraction Juhan3 = new Lfraction(-4, 7);
//		System.out.println(gcd(-4,7));
//		Lfraction Juhan4 = new Lfraction(5, -6);
//		Lfraction Juhan5 = new Lfraction(-2, 5);

//		 System.out.println("Taandamischeck 2/4 = " + Juhan2);
//		 System.out.println("arv 1 = " + Juhan);
//		 System.out.println("arv 2 = " + Juhan3);
////		 System.out.println(Juhan2.equals(Juhan));
//		 System.out.println(Juhan.plus(Juhan3) + "   PEAB OLEMA 2/3");
//		 System.out.println(" peab olema -5 / 6: " + Juhan4);
//		System.out.println("adfjaspoifjaspdfj " + gcd(-2, 5));
//		System.out.println("plaks" + " " + Juhan3);
//		System.out.println("pliks" + " " + Juhan);
		System.out.println(Juhan);
		System.out.println();
//		 System.out.println("-2 / 5 compareto -4 / 7 must be true: " + Juhan.compareTo(Juhan3));
//		 System.out.println(Juhan.minus(Juhan3));
//		 System.out.println(Juhan4.plus(Juhan2));
//		 System.out.println(Juhan.times(Juhan3));
//		 System.out.println(Juhan.divideBy(Juhan3));
//		 System.out.println(Juhan.divideBy(Juhan4));
////		 System.out.println(Juhan.divideBy(Juhan5));
//		 System.out.println(valueOf("5 / 6"));
//		 System.out.println(valueOf("5   / 6"));
//		 System.out.println(valueOf("5 / -25"));
//		 System.out.println(valueOf("5 / -25 / 5"));
	}

	private long numenator; //Lugeja
	private long denominator; //Nimetaja

	public Lfraction(long a, long b) {
		if (b == 0) {
			throw new RuntimeException("Divide by 0 error");
		}

		long gcd;
		if (gcd(a, b) < 0) {
		gcd = -gcd(a, b);
			
		}
		else {
		gcd = gcd(a, b);
		}

		if (b < 0) {
			numenator = -(a / gcd);
			denominator = -(b / gcd);

		} else {
			numenator = (a / gcd) ;
			denominator = (b / gcd) ;
		}
		
	}

	public long getNumerator() {
		return numenator;
	}

	public long getDenominator() {
		return denominator;
	}

	@Override
	public String toString() {
		return String.format("%d / %d", numenator, denominator);
	}

	@Override
	public boolean equals(Object m) {
		Lfraction n = this.reduce();
		return ((Lfraction) m).reduce().denominator == n.denominator && ((Lfraction) m).reduce().numenator == n.numenator;
	}

	@Override
	public int hashCode() {
		return (int) ((numenator * 1000) + denominator);
	}

	public Lfraction plus(Lfraction m) {
		Lfraction vastus = new Lfraction(this.numenator * m.denominator + m.numenator * denominator, m.denominator * denominator);
		return vastus;
	}

	public Lfraction times(Lfraction m) {
		Lfraction result = new Lfraction(numenator * m.numenator, denominator * m.denominator);
		return result.reduce().reduce();
	}

	public Lfraction inverse() {
		if (this.numenator == 0) {
			throw new RuntimeException("Divide by 0 error");
		}
		return new Lfraction(denominator, numenator);
	}

	public Lfraction opposite() {
		return new Lfraction(-numenator, denominator);
	}

	public Lfraction minus(Lfraction m) {

		Lfraction vastus = new Lfraction(this.numenator * m.denominator - m.numenator * denominator, m.denominator * denominator);
		return vastus;
	}

	public Lfraction divideBy(Lfraction m) {
		if (m.denominator == 0) {
			throw new RuntimeException("Divide by 0 error");
		}
		return times(m.inverse()).reduce();
	}

	public int compareTo(Lfraction m) {
		
		long first = numenator * m.denominator;
//		System.out.println();
//		System.out.println("compareto numerato " + numenator);
//		System.out.println("compareto denomi " + m.denominator);
//		System.out.println(first + " esimene arv");
		long second = denominator * m.numenator;
//		System.out.println(second + " teine arv");
		if (first > second) {
			return 1;
		}
		if (first < second) {
			return -1;
		}
		return 0;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return new Lfraction(numenator, denominator);
	}

	public long integerPart() {
		return numenator / denominator;
	}

	public Lfraction fractionPart() {
		if (integerPart() == 0) {
			return this;
		} else {
			long fraction = numenator - (integerPart() * denominator);
			return new Lfraction(fraction, denominator);
		}
	}

	public double toDouble() {
		return (double) numenator / (double) denominator;
	}
	
    public static Lfraction toLfraction(double f, long d) {
        long t2isarv = StrictMath.round(d*f);
        return new Lfraction(t2isarv, d).reduce();
    }

	public static Lfraction valueOf(String s) {
		try {
		    char toCheck = '/';
		    int count = 0;

		    for (char ch: s.toCharArray()) { 
		        if (ch == toCheck) {
		            count++;
		        }
		    }
		    if (count != 1) {
		    	throw new IllegalArgumentException("Input: \""  + s + "\" contains too many / symbols");
				
			}
		    
			if (!s.matches(".*\\d+.*" + "/" + ".*\\d+.*")) {
				throw new IllegalArgumentException("Input: \""  + s + "\" is not proper");
			}
			if (!s.contains(" / ")) {
				throw new IllegalArgumentException("Input: \""  + s + "\" is missing a space before and/or after the fraction symbol");
			}
			String[] stringid = s.trim().split(" / ");		
			if (!stringid[0].trim().matches(".*\\d+.*") || !stringid[1].trim().matches(".*\\d+.*")) {
				throw new IllegalArgumentException("Input: \""  + s + "\" is not proper");
			}
			long lugeja = Long.parseLong(stringid[0].trim());
			
			long nimetaja = Long.parseLong(stringid[1].trim());
			return new Lfraction(lugeja, nimetaja);
			
		} catch (Exception NumberFormatException) {
			throw new NumberFormatException("Avaldises ei tohi esineda t�hti, ega liigseid s�mboleid, Avaldis on vale: \"" + s + "\"");
		}
	}

	private Lfraction reduce() {
		long gcd = gcd(numenator, denominator);
		return new Lfraction(numenator / gcd, denominator / gcd);
	}

	public static long gcd(long a, long b) {
		if (b == 0)
			return a;
		return gcd(b, a % b);
	}

}

// Viide: https://pastebin.com/XX2s6WZQ